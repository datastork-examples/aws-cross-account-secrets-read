package com.datastork.example;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;

import java.util.Map;

public class CrossAccountSecretsReadLambda implements RequestHandler<Map<String, Object>, String> {

    private final AWSSecretsManager awsSecretsManager;

    public CrossAccountSecretsReadLambda() {
        awsSecretsManager = AWSSecretsManagerClientBuilder
                .standard()
                .build();
    }

    @Override
    public String handleRequest(Map<String, Object> input, Context context) {
        GetSecretValueRequest getSecretValueRequest =
                new GetSecretValueRequest().withSecretId("arn:aws:secretsmanager:eu-west-1:CENTRAL_ACCOUNT_ID:secret:central-password-storage-ebK48h");

        return awsSecretsManager.getSecretValue(getSecretValueRequest).getSecretString();
    }

}
